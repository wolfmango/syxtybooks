package book

import (
	"fmt"
)

type (
	ParsingStrategy struct {
	}

	BookSource struct {
		BaseURL         string
		ParsingStrategy ParsingStrategy
	}

	Chapter struct {
	}

	Book struct {
		ID       int
		Name     string
		Authors  []string
		Cover    string
		Chapters []*Chapter
		Source   BookSource
	}
)

func (b *Book) Description() string {
	return fmt.Sprintf("--- Book ---\n%s\nID: %d\nAuthors: %s\n# of Chapters: %d\n---",
		b.Name, b.ID, b.Authors, len(b.Chapters))
}
