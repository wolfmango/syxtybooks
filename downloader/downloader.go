package downloader

import (
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/html"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

type Downloader struct {
	url        string
	maxRetries int
}

const (
	maxRetries = 5
)

func New(url string) *Downloader {
	return &Downloader{
		url:        url,
		maxRetries: 5,
	}
}

func (d *Downloader) Download(chFinished chan *html.Node, chErrs chan error) {
	var resp *http.Response
	retries := 0
	for true {
		resp, err := http.Get(d.url)
		if err == nil {
			defer resp.Body.Close()
			break
		}
		if retries >= maxRetries {
			chErrs <- fmt.Errorf("failed downloading %v: %v", d.url, err)
			return
		}
		time.Sleep(10 * time.Second)
		retries++
	}
	r := transform.NewReader(resp.Body, simplifiedchinese.GBK.NewDecoder())
	doc, err := html.Parse(r)
	if err != nil {
		chErrs <- fmt.Errorf("failed to parse %v: %v", d.url, err)
		return
	}
	chFinished <- doc
}
