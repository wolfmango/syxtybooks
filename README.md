# Syxty books

This project is started as a way to parse e-books from `69shu.com` and deliver
to a kindle.

## Requirements

- Bazel
- Go `1.9.1`
- Docker
- ebook-convert `calibre 3.12.0`
